<?php include ('header.php');
include_once ('config.php');
include ('functions/ad_functions.php');
$client=  GET_CLIENT_LIST();
if(isset($_GET['ad_id']))
{
    $get_ad=GET_AD_BY_ID($_GET['ad_id']);
    $ad_info=  mysql_fetch_array($get_ad);
}
?>
<script>
    function validateForm()
    {
        var client=document.getElementById('selectclient').value;
        if (client==null || client=="") {
            document.getElementById("clientBox").innerHTML = "please select a client";
            document.getElementById("selectclient").focus();
            return false;
        }
        var file=document.getElementById('upfile').value;
        <?php if(isset($_GET['ad_id'])){
            
            }else{ ?>
        if (file==null || file=="") {
            document.getElementById("fileBox").innerHTML = "please select a file";
            document.getElementById("upfile").focus();
            return false;
        }
            <?php } ?>
        var extension = file.substr(file.lastIndexOf('.') + 1).toLowerCase();
        var allowedExtensions = ['jpg', 'jpeg', 'png', 'html', 'htm'];
        if (file.length > 0)
        {
            if (allowedExtensions.indexOf(extension) === -1)
            {
                document.getElementById("fileBox").innerHTML = 'Invalid file Format. Only ' + allowedExtensions.join(', ') + ' are allowed.';
                document.getElementById("upfile").focus();
                return false;
            }
        }
        var s_date=document.getElementById('date01').value;
        if (s_date==null || s_date=="") {
            document.getElementById("startBox").innerHTML = "please select a date";
            document.getElementById("date01").focus();
            return false;
        }
        var e_date=document.getElementById('date02').value;
        if (e_date==null || e_date=="") {
            document.getElementById("expiryBox").innerHTML = "please select a date";
            document.getElementById("date02").focus();
            return false;
        }
        var numbers = /^[0-9]+$/;
        var maxv=document.getElementById('max_view').value;
        if (maxv==null || maxv=="") {
            document.getElementById("mxBox").innerHTML = "enter maximum view";
            document.getElementById("max_view").focus();
            return false;
        } else if (!maxv.match(numbers)){
            document.getElementById("mxBox").innerHTML = "only numbers";
            document.getElementById("max_view").focus();
            return false;
        }
        var maxvpd=document.getElementById('max_view_per_day').value;
        if (maxvpd==null || maxvpd=="") {
            document.getElementById("mxpdBox").innerHTML = "enter maximum view per day";
            document.getElementById("max_view_per_day").focus();
            return false;
        } else if (!maxvpd.match(numbers)){
            document.getElementById("mxpdBox").innerHTML = "only numbers";
            document.getElementById("max_view_per_day").focus();
            return false;
        }
        var money=document.getElementById('money_per_view').value;
        if (money==null || money=="") {
            document.getElementById("moneyBox").innerHTML = "enter money per view";
            document.getElementById("money_per_view").focus();
            return false;
        } else if (!money.match(numbers)){
            document.getElementById("moneyBox").innerHTML = "only numbers";
            document.getElementById("money_per_view").focus();
            return false;
        }
    }
</script>
<script>
    function clean_error(id)
    {
        document.getElementById(id).innerHTML = "";
    }
</script>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="add_client.php">Add Client</a>
            </li>
        </ul>
        <hr>
    </div>   	
    <div style="color: red;text-align: center"><?php if(isset($msg)!='') { echo $msg; } ?>	</div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2><i class="fa fa-edit"></i><?php if(isset($_GET['ad_id'])){ ?>Edit<?php }else{ ?>Add<?php } ?> Advertise</h2>
                    <div class="box-icon">
                        
                    </div>
                </div>
                <div class="box-content">
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $ad_info['ad_id'] ?>">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <tr>
                                <td style="width: 20%;">
                                    <label class="control-label">Select Client<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <select id="selectclient" name="selectclient" onchange="clean_error('clientBox')" class="form-control" style="width: 40%;">
                                        <option value="">--Select Client--</option>
                                        <?php while ($client_list = mysql_fetch_array($client)) { ?>
                                        <option value="<?php echo $client_list[0]; ?>" <?php if($client_list[0]==$ad_info['c_id']) { ?>selected="selected"<?php } ?> ><?php echo $client_list[1]; ?></option>     
                                        <?php } ?>
                                    </select>
                                    <span id="clientBox" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Upload File<span style="color: red">*</span></label>
                                </td>
                                <td><?php if(isset($_GET['ad_id'])){ 
                                    $extn=explode(".", strtolower($ad_info['attached_file_path']));
                                    if($extn[1]=='jpg' || $extn[1]=='jpeg' || $extn[1]=='png') { ?>
                                            <img src="<?php echo $DIR_PATH; ?>functions/ad_files/<?php echo $ad_info['attached_file_path']; ?>" height="80" width="80"/> 
                                   <?php }elseif($extn[1]=='htm' || $extn[1]=='html'){ ?>
                                            <a target="_blank" href="<?php echo $DIR_PATH; ?>functions/ad_files/<?php echo $ad_info['attached_file_path']; ?>"><?php $fname=explode("f",$ad_info['attached_file_path']); echo $fname[1]; ?></a>
                                  <?php }else{ ?>
                                            <video width="200" controls>
                                                <source src="<?php echo $DIR_PATH; ?>functions/ad_files/<?php echo $ad_info['attached_file_path']; ?>" type="">
                                                Your browser does not support HTML5 video.
                                            </video>
                                      <?php } ?>
                               <?php } ?>
                                    <input type="file" id="upfile" onchange="clean_error('fileBox')" name="upfile"/>
                                    <span id="fileBox" style="color: red"></span>
                                    <p class="help-block"><?php //if(isset($_GET['ad_id'])){ echo 'Old File='.$ad_info['attached_file_path']; }else{ ?>Only (Image/HTML)<?php// } ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Start Date<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <div class="input-group date col-sm-4">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control date-picker" onchange="clean_error('startBox')" id="date01" name="start_date" data-date-format="dd-mm-yyyy" <?php if(isset($_GET['ad_id'])) { ?>value="<?php echo date('d-m-Y',strtotime($ad_info['start_date'])); ?>"<?php } ?> />
                                        <span id="startBox" style="color: red"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Expiry Date<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <div class="input-group date col-sm-4">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control date-picker" onchange="clean_error('expiryBox')" id="date02" name="expiry_date" data-date-format="dd-mm-yyyy" <?php if(isset($_GET['ad_id'])) { ?>value="<?php echo date('d-m-Y',strtotime($ad_info['expiry_date'])); ?>"<?php } ?> />
                                        <span id="expiryBox" style="color: red"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Maximum View<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" type="text" onkeypress="clean_error('mxBox')" id="max_view" name="max_view" style="width: 40%;" <?php if(isset($_GET['ad_id'])) { ?>value="<?php echo $ad_info['max_view_for_ad'] ?>"<?php } ?> />
                                    <span id="mxBox" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Maximum View per Day<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" type="text" onkeypress="clean_error('mxpdBox')" id="max_view_per_day" name="max_view_per_day" style="width: 40%;" <?php if(isset($_GET['ad_id'])) { ?>value="<?php echo $ad_info['max_view_per_day'] ?>"<?php } ?> />
                                    <span id="mxpdBox" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Money per View<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" type="text" onkeypress="clean_error('moneyBox')" id="money_per_view" name="money_per_view" style="width: 40%;" <?php if(isset($_GET['ad_id'])) { ?>value="<?php echo $ad_info['money_per_view'] ?>"<?php } ?> />
                                    <span id="moneyBox" style="color: red"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td><?php
                                if(isset($_GET['ad_id'])){ ?>
                                    <input type="submit" class="btn btn-primary" onclick="return validateForm()" name="update" value="Update"/>
                                <?php }else{ ?>
                                    <input type="submit" class="btn btn-primary" onclick="return validateForm()" name="submit" value="Submit"/>
                                   <?php } ?>
                                    <a href="index.php" class="btn">Cancel</a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>    
</div>
<?php
if(isset($_POST['submit']))
{
    $insert_ad=  INSERT_AD($_POST, $_FILES['upfile']);
    if($insert_ad>0)
    {
        $_SESSION['msg']='Successfully inserted';
        $url=$DIR_PATH."manage_ad.php";
        echo "<script>window.top.location='".$url."'</script>";
    }else{
        $_SESSION['msg']='Error in Ad insertion';
        $url=$DIR_PATH."manage_ad.php";
        echo "<script>window.top.location='".$url."'</script>";
    }
}
if(isset($_POST['update']))
{
    $update_ad=UPDATE_AD($_POST,$_FILES['upfile']);
    if($update_ad>0)
    {
        $_SESSION['msg']='Ad updated Successfully !';
        $url=$DIR_PATH."manage_ad.php";
        echo "<script>window.top.location='".$url."'</script>";
    }else{
        $_SESSION['msg']='Error in Ad updation !';
        $url=$DIR_PATH."manage_ad.php";
        echo "<script>window.top.location='".$url."'</script>";
    }
}
?>
<?php include ('footer.php');?>