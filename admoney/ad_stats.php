<?php
include ('header.php');
include_once ('config.php');
include ('functions/ad_functions.php');
$ads=  GET_AD_LIST();
?>
<div id="content" class="col-sm-10">
    
        <div>
            <hr>
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="ad_stats.php">Ad List</a>
                </li>
            </ul>
            <hr>
        </div>
        <div class="row">
        <div class="col-lg-12">
    <div class="form-group">
        <label class="control-label" for="selectError">Search By:-</label>
        <div class="controls">
            <select id="selectError" class="form-control" data-rel="chosen" style="width: 25%;" onchange="document.location.href = 'ad_stats.php?search_by='+ this.value" >
                <option value="all">--Select--</option>
                <option value="week" <?php if($_GET['search_by']=='week'){ ?>selected<?php } ?> >Past Week</option>
                <option value="month" <?php if($_GET['search_by']=='month'){ ?>selected<?php } ?> >Past Month</option>
                <option value="date" <?php if($_GET['search_by']=='date'){ ?>selected<?php } ?> >Specific Date</option>
            </select>
        </div>
    </div>
        </div>
    </div>
    <?php if($_GET['search_by']=='date' && empty($_GET['date'])){ ?>
    <fieldset class="col-sm-12">
        <div class="form-group">
            <label class="control-label" for="date01">Date</label>
            <div class="controls row">
                <div class="input-group date col-sm-4">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control date-picker" id="date01" data-date-format="dd-mm-yyyy" style="width: 72%;" onchange="document.location.href = 'ad_stats.php?search_by=date&date='+this.value" />
                </div>
            </div>
        </div>
    </fieldset>
    <?php } ?>
        <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Ad Statistics <?php if($_GET['search_by']=='week'){ echo "- Last 7 days"; } elseif($_GET['search_by']=='month'){ echo "- Last Month"; } elseif(isset($_GET['date'])){ echo "- On ".$_GET['date']; } ?></h2>
                    <div class="box-icon">
                        <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Number of views</th>
                                <th>Remaining views</th>
                                <th>Total views</th>
                                <th>Expire On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($ads_list = mysql_fetch_array($ads)) { ?>                       
                            <tr>
                                <td>
                                    <?php echo GET_CLIENT_BY_ID($ads_list['c_id']); ?>
                                </td>
                                <td>
                                    <?php 
                                    $views=GET_VIEW_BY_ID($ads_list['ad_id']);
                                    $views_no=mysql_num_rows($views);
                                    if($_GET['search_by']=='week'){
                                        $week_view=  GET_VIEW_OF_WEEK($ads_list['ad_id']);
                                        echo mysql_num_rows($week_view);
                                    }  elseif ($_GET['search_by']=='month') {
                                        $month_view= GET_VIEW_OF_MONTH($ads_list['ad_id']);
                                        echo mysql_num_rows($month_view);
                                    } elseif (isset($_GET['date'])) {
                                        $date_view=GET_VIEW_OF_DATE($ads_list['ad_id'],$_GET['date']);
                                        echo mysql_num_rows($date_view);
                                    } else {
                                        echo $views_no;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    $remain=$ads_list['max_view_for_ad']-$views_no;
                                    echo $remain;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $ads_list['max_view_for_ad']; ?>
                                </td>
                                <td>
                                    <?php
                                    $expiry_date=  date('F d, Y',strtotime($ads_list['expiry_date']));
                                    echo $expiry_date;
                                    ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include ('footer.php');
?>
