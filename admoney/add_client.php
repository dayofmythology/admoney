<?php include ('header.php');
include_once ('config.php');
include ('functions/client_functions.php');
if(isset($_GET['c_id']))
{
    $info=GET_CLIENT_INFO($_GET['c_id']);
    $client_detail=  mysql_fetch_array($info);
}
?>
<script>
function validateForm() {
    var letters = /^[A-Za-z ]+$/;
    var uname = document.forms["form1"]["uname"].value;
    if (uname==null || uname=="") {
        document.getElementById("errorBox").innerHTML = "enter name";
        document.getElementById("uname").focus();
        return false;
    } else if (!uname.match(letters)){
        document.getElementById("errorBox").innerHTML = "only letters";
        document.getElementById("uname").focus();
        return false;
    }
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var uemail = document. forms["form1"]["uemail"].value;
    if (uemail==null || uemail==""){
        document.getElementById("erroremail").innerHTML = "email required";
        document.getElementById("uemail").focus();
        return false;
    } else if (!uemail.match(reg)){
        document.getElementById("erroremail").innerHTML = "enter correct format";
        document.getElementById("uemail").focus();
        return false;
    }
    var contact=document.getElementById('contact_number').value;
    if(contact != ''){
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        if( !phoneno.test(contact) ) {
            document.getElementById("errorcontact").innerHTML = "enter a valid number";
            document.getElementById("contact_number").focus();
            return false;
        }
    }
    var weburl=document.getElementById('website_url').value;
    var validurl = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if (weburl==null || weburl=="") {
        document.getElementById("errorurl").innerHTML = "enter website url";
        document.getElementById("website_url").focus();
        return false;
    } else if (!validurl.test(weburl)){
        document.getElementById("errorurl").innerHTML = "enter a valid url";
        document.getElementById("website_url").focus();
        return false;
    }
}
</script>
<script>
    function clean_error(id)
    {
        document.getElementById(id).innerHTML = "";
    }
</script>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="add_advertise.php">Add Advertise</a>
            </li>
        </ul>
        <hr>
    </div>   	
    <div style="color: red;text-align: center"><?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; unset($_SESSION['msg']); } ?></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h2><i class="fa fa-edit"></i><?php if(isset($_GET['c_id'])){ ?>Edit<?php }else{ ?>Add<?php } ?> Client</h2>
                    <div class="box-icon">
                        
                    </div>
                </div>
                <div class="box-content">
                    <form name="form1" id="form1" action="" method="post">
                        <input type="hidden" name="client_id" value="<?php echo $client_detail['c_id'] ?>"/>
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <tr>
                                <td style="width: 20%;">
                                    <label class="control-label">Name<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" onkeypress="clean_error('errorBox')" id="uname" name="uname" type="text" <?php if(isset($_GET['c_id'])){ ?>value="<?php echo $client_detail['name']; ?>"<?php } ?> />
                                    <span id="errorBox" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Address</label>
                                </td>
                                <td>
                                    <input class="form-control focused" id="address" name="address" type="text" <?php if(isset($_GET['c_id'])){ ?>value="<?php echo $client_detail['address']; ?>"<?php } ?> />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Email<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" id="uemail" onkeypress="clean_error('erroremail')" name="uemail" type="text" <?php if(isset($_GET['c_id'])){ ?>value="<?php echo $client_detail['email']; ?>"<?php } ?> />
                                    <span id="erroremail" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Contact Number</label>
                                </td>
                                <td>
                                    <input class="form-control focused" onkeypress="clean_error('errorcontact')" id="contact_number" name="contact_number" type="text" <?php if(isset($_GET['c_id'])){ ?>value="<?php echo $client_detail['contact_no']; ?>"<?php } ?> />
                                    <span id="errorcontact" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="control-label">Website Url<span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input class="form-control focused" onkeypress="clean_error('errorurl')" id="website_url" name="website_url" type="text" <?php if(isset($_GET['c_id'])){ ?>value="<?php echo $client_detail['weburl']; ?>"<?php } ?> />
                                    <span id="errorurl" style="color: red"></span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><?php
                                if(isset($_GET['c_id'])){ ?>
                                    <input type="submit" class="btn btn-primary" onclick="return validateForm()" name="update" value="Update"/>
                                <?php }else{ ?>
                                    <input type="submit" class="btn btn-primary" onclick="return validateForm()" name="submit" value="Submit"/>
                                   <?php } ?>
                                    <a href="index.php" class="btn">Cancel</a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>    
</div>
<?php
if(isset($_POST['submit'])!='')
{
    $add_client=  INSERT_CLIENT($_POST);
    if($add_client>0)
    {
        $_SESSION['msg']="Client inserted Successfully !";
        $url=$DIR_PATH."clients.php";
        echo "<script>window.top.location='".$url."'</script>";
    }else{
        $_SESSION['msg']="Error in client insertion !";
        $url=$DIR_PATH."clients.php";
        echo "<script>window.top.location='".$url."'</script>";
    }
}
if(isset($_POST['update'])){
    $update_client= UPDATE_CLIENT($_POST);
    if($update_client>0)
    {
        $_SESSION['msg']="Client updated Successfully !";
        $url=$DIR_PATH."clients.php";
        echo "<script>window.top.location='".$url."'</script>";
    }else{
        $_SESSION['msg']="Error in client updation !";
        $url=$DIR_PATH."clients.php";
        echo "<script>window.top.location='".$url."'</script>";
    }
}
?>
<?php include ('footer.php');?>
