<?php include ('header.php'); ?>
<?php
include_once ('config.php');
include ('functions/user_functions.php');
$u_list=  GET_USER_LIST();
?>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="users_list.php">Users List</a>
            </li>
        </ul>
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Users List</h2>
                    <div class="box-icon">
                        <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Mobile Number</th>
                                    <th>Registered through</th>
                                    <th>Registration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($get_list=  mysql_fetch_array($u_list)){ ?>
                                <tr>
                                    <td><?php echo $get_list['user_full_name']; ?></td>
                                    <td><?php echo $get_list['email']; ?></td>
                                    <td>
                                        <a href="<?php echo $DIR_PATH; ?>webservice/user_pics/<?php echo $get_list['image_url']; ?>" data-lightbox="<?php echo $get_list['image_url']; ?>" data-title="<?php echo $get_list['user_full_name']; ?> Image">
                                        <img src="<?php echo $DIR_PATH; ?>webservice/user_pics/<?php echo $get_list['image_url']; ?>" height="80" width="70">
                                        </a>
                                    </td>
                                    <td><?php echo $get_list['mobile_number']; ?></td>
                                    <td><?php 
                                    $type=$get_list['login_type'];
                                    if($type==0)
                                    {
                                        echo "Mobile";
                                    }elseif ($type==1) {
                                        echo "Facebook";
                                    }
                                    ?></td>
                                    <td><?php
                                    $date=explode(" ",$get_list['register_date']);
                                    echo date('d-m-Y',strtotime($date[0]));
                                    ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>


<?php include ('footer.php'); ?>

