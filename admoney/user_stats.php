<?php include ('header.php'); ?>
<?php
include_once ('config.php');
include ('functions/user_functions.php');
if($_GET['action']=='status')
{
	$result=UPDATE_STATUS_BYID($_GET['uid'],$_GET['status']);
	if($result > 0)
	{
		$msg="Status update successfully !";
	} 
	
}
if($_GET['action']=='delete')
{
$del=DELETE_USER_BY_ID($_GET['uid']);
if($del>0)
{
    $msg="Record deleted successfully !";
}
}
$u_list=  GET_USER_LIST();
?>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="user_stats.php">Users List</a>
            </li>
        </ul>
        <hr>
    </div>
    <div style="color: red;text-align: center"><?php if(isset($msg)!='') { echo $msg; } ?></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Users List</h2>
                    <div class="box-icon">
                        <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Mobile Number</th>
                                    <th>Registered through</th>
                                    <th>Registration Date</th>
                                    <th>Total View</th>
                                    <th>Total Earning</th>
                                    <th>Status</th>
                                    <th style="width: 13%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($get_list=  mysql_fetch_array($u_list)){ ?>
                                <tr>
                                    <td><?php echo $get_list['user_full_name']; ?></td>
                                    <td><?php echo $get_list['email']; ?></td>
                                    <td>
                                        <a href="<?php echo $PATH; ?>webservice/user_pics/<?php echo $get_list['image_url']; ?>" data-lightbox="<?php echo $get_list['image_url']; ?>" data-title="<?php echo $get_list['user_full_name']; ?> Image">
                                        <img src="<?php echo $PATH; ?>webservice/user_pics/<?php echo $get_list['image_url']; ?>" height="80" width="70">
                                        </a>
                                    </td>
                                    <td><?php echo $get_list['mobile_number']; ?></td>
                                    <td><?php 
                                    $type=$get_list['login_type'];
                                    if($type==0)
                                    {
                                        echo "Email";
                                    }elseif ($type==1) {
                                        echo "Facebook";
                                    }
                                    ?></td>
                                    <td><?php
                                    $date=explode(" ",$get_list['register_date']);
                                    echo date('d-m-Y',strtotime($date[0]));
                                    ?></td>
                                    <td>
                                        <?php
                                        $detail=GET_AD_DETAIL($get_list['uid']);
                                        $no_of_views= mysql_num_rows($detail);
                                        echo $no_of_views;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $total=0;
                                        while($money=  mysql_fetch_array($detail))
                                        {
                                            $total=$total+$money['money_earned'];
                                        }
                                        echo $total;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if($get_list['status']==1){ ?>
                                        <a title="Change Status" href="user_stats.php?action=status&uid=<?=$get_list['uid']?>&status=0"><span class="label label-success">Active</span></a>
                                       <?php } else { ?>
                                        <a title="Change Status" href="user_stats.php?action=status&uid=<?=$get_list['uid']?>&status=1"><span class="label label-default">Inactive</span></a>
                                       <?php } ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="details.php?id=<?php echo $get_list['uid'] ?>"><i class="fa fa-search-plus "></i></a>
                                        <a title="Delete" class="btn btn-danger" onClick="if(!confirm('Are you sure, You want delete this record?')){return false;}" href="user_stats.php?action=delete&uid=<?=$get_list['uid']?>">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>


<?php include ('footer.php'); ?>

