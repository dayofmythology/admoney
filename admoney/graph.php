<?php
include('config.php');
for ($i = 6 ; $i >= 0 ; $i--) {
    $week[$i]= date('Y-m-d',  strtotime("-$i days"));
    $sql="Select * from ad_view_flag where date_created = '$week[$i]'";
    $result=  mysql_query($sql)or die(mysql_error());
    $no_of_views = mysql_num_rows($result);
    $data[date('d-M-Y', strtotime($week[$i]))]=$no_of_views;
}
include('phpgraphlib.php');
$graph = new PHPGraphLib(1000,400);
$graph->addData($data);
$graph->setTitle('Statistics');
$graph->setTitleLocation('left');
$graph->setLegend(true);
$graph->setDataPoints(true);
$graph->setLine(true);
$graph->setBars(false);
$graph->setTitleColor('blue');
$graph->setDataValues(true);
$graph->setGridColor('153,204,255');
$graph->setXValuesHorizontal(true);
$graph->setLegendTitle('Past Week');
$graph->setGradient('teal', '#0000FF');
$graph->createGraph();