<?php include ('header.php');
include_once ('config.php');
include ('functions/ad_functions.php');
$ad_list=  GET_AD_LIST();
?>
<script>
    $(document).ready(function (){
       $('#ad_Table').dataTable({ "bLengthChange": true,
"aoColumnDefs": [{ "bSearchable": false, "aTargets": [6]}] // this line does your work!
});
    });
</script>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="manage_ad.php">AD List</a>
            </li>
        </ul>
        <hr>
    </div>
    <div style="color: red;text-align: center"><?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; unset($_SESSION['msg']); } ?>	</div>
    <a title="Add New Advert" class="btn btn-primary" href="add_advertise.php"><span class="hidden-sm">Add Adverts</span></a>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Ad List</h2>
                    <div class="box-icon">
                        <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                    <div class="box-content">
                        <table id="ad_Table" class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>Start Date</th>
                                    <th>Expiry Date</th>
                                    <th>Maximum View</th>
                                    <th>Maximum View per Day</th>
                                    <th>Money per View</th>
                                    <th>Status</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($get_list=  mysql_fetch_array($ad_list)){ 
                                //    $check_count=GET_MAXCOUNT_LIST($get_list['ad_id']);
                                //    $check_count_perday=GET_MAXCOUNT_PER_DAY($get_list['ad_id']);
                                //    if($check_count['no_of_views']>$get_list['max_view_for_ad']){
                                        
                                //    }elseif($check_count_perday['no_of_views_a_day']>$get_list['max_view_per_day']) {} else { ?>
                                <tr>
                                    <td><?php $client_name=GET_CLIENT_BY_ID($get_list['c_id']); echo $client_name; ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($get_list['start_date'])); ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($get_list['expiry_date'])); ?></td>
                                    <td><?php echo $get_list['max_view_for_ad']; ?></td>
                                    <td><?php echo $get_list['max_view_per_day']; ?></td>
                                    <td><?php echo $get_list['money_per_view']; ?></td>
                                    <td>
                                        <?php
                                        if($get_list['status']==1){ ?>
                                        <a title="Change Status" href="manage_ad.php?action=status&ad_id=<?=$get_list['ad_id']?>&status=0"><span class="label label-success">Active</span></a>
                                       <?php } else { ?>
                                        <a title="Change Status" href="manage_ad.php?action=status&ad_id=<?=$get_list['ad_id']?>&status=1"><span class="label label-default">Inactive</span></a>
                                       <?php } ?>                       
                                    </td>
                                    <td>
                                        <a title="Edit" class="btn btn-info" href="add_advertise.php?ad_id=<?=$get_list['ad_id']?>">
                                            <i class="fa fa-edit "></i>
                                        </a>
                                        <a title="Delete" class="btn btn-danger" onClick="if(!confirm('Are you sure, You want delete this record?')){return false;}" href="manage_ad.php?action=delete&ad_id=<?=$get_list['ad_id']?>">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </td>
                                    <!--<td colspan="6"> Table will be displayed here!</td>-->
                                </tr>
                                <?php //} ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<?php
if($_GET['action']=='status')
{
	$result=UPDATE_STATUS_BYID($_GET['ad_id'],$_GET['status']);
	if($result > 0)
	{
                $_SESSION['msg']='Status update successfully !';
                $url=$DIR_PATH."manage_ad.php";
                echo "<script>window.top.location='".$url."'</script>";
	}
}
if($_GET['action']=='delete')
{
$del=DELETE_AD_BY_ID($_GET['ad_id']);
if($del > 0)
{
    $_SESSION['msg']='Record deleted successfully !';
    $url=$DIR_PATH."manage_ad.php";
    echo "<script>window.top.location='".$url."'</script>";
}elseif($del == FALSE) {
    $_SESSION['msg']="Could not delete this Advert !!<br>&nbsp;Many user have seen this Ad !!";
    $url=$DIR_PATH."manage_ad.php";
    echo "<script>window.top.location='".$url."'</script>";
}
}
?>
<?php include ('footer.php'); ?>

