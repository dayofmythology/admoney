<?php
session_start();
if(!isset($_SESSION['user']))
{
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Ad Money Dashboard</title>
	<meta name="description" content="Ad Money Dashboard">
	<meta name="author" content="prashant">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	
	<!-- bootstrap -->
        <link href="assets/css/bootstrap.min_1.css" rel="stylesheet">
	
	<!-- page css files -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
        <link href="assets/css/chosen.min.css" rel="stylesheet">
        <link href="assets/css/jquery.cleditor.css" rel="stylesheet">
        <link href="assets/css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="assets/css/bootstrap-timepicker.css" rel="stylesheet">
        <link href="assets/css/daterangepicker.css" rel="stylesheet">
	<link href="assets/css/bootstrap-colorpicker.css" rel="stylesheet">
        <link href="http://localhost:8888/bootstrap/perfectum2/assets/css/dataTables.css" rel="stylesheet">
	<link href="assets/css/fullcalendar.css" rel="stylesheet">	
	<!--<link href="assets/css/jquery.gritter.css" rel="stylesheet">-->	
        <link href="assets/css/lightbox.css" rel="stylesheet" />
	<!-- main style -->
	<link href="assets/css/style.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
	<!--[if lt IE 9 ]>
		<link href="assets/css/style-ie.css" rel="stylesheet">
	<![endif]-->
	
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="assets/js/respond.min.js"></script>
		
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="http://localhost:8888/bootstrap/perfectum2/img/favicon.ico">
	<!-- end: Favicon -->
	
		
<style type="text/css">
    .pagination{
        float: right;
    }
.dataTables_filter {
float: right;
}
</style>		
   


</head>

<body>
	
	<!-- start: Header -->
	<div class="navbar">
		<div class="container">
			
			<a class="navbar-brand" href="index.php"> <img alt="Perfectum Dashboard" src="assets/img/logo20.png" /> <span>Ad Money</span></a>
							
			<!-- start: Header Menu -->
			<div class="header-nav">
				<ul class="nav navbar-nav pull-right">
					
					<!-- start: User Dropdown -->
					<li class="dropdown hidden-xs">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="index.html#">
							<i class="fa fa-user"></i>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
                                                        <li><a href="change_pass.php"><i class="fa fa-cog"></i>Change Password</a></li>
                                                        <li><a href="index.php?action=logout"><i class="fa fa-power-off"></i> Logout</a></li>
						</ul>
					</li>
					<!-- end: User Dropdown -->
				</ul>
			</div>
			<!-- end: Header Menu -->
			
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container">
		<div class="row">
					<!-- start: Main Menu -->
			<div class="col-sm-2 main-menu-span">
				<div class="sidebar-nav nav-collapse collapse navbar-collapse">
					<ul class="nav nav-tabs nav-stacked main-menu">
                                            <li><a href="dashboard.php"><i class="fa fa-home icon"></i><span class="hidden-sm"> Dashboard</span></a></li>
                                            <li><a href="clients.php"><i class="fa fa-list"></i><span class="hidden-sm"> Clients</span></a></li>
                                            <li><a href="manage_ad.php"><i class="fa fa-list-alt"></i><span class="hidden-sm"> Adverts</span></a></li>
                                            <li><a href="user_stats.php"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm">Users Statistics</span></a></li>
                                            <li><a href="ad_stats.php"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm">Adverts Statistics</span></a></li>
					</ul>
				</div><!--/.well -->
			</div><!--/col-->
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block col-sm-10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
