<?php 
include ('header.php'); 
include ('config.php');
include ('functions/user_functions.php');
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $detail = GET_DETAIL($id);
}
if($_GET['search_by']=='all')
{
    $detail = GET_DETAIL($id);
}
if($_GET['search_by']=='week')
{
    $detail = GET_DETAIL_WEEK($id);
}
if($_GET['search_by']=='month')
{
    $detail = GET_DETAIL_MONTH($id);
}
if(isset($_GET['date'])){
    $detail= GET_DETAIL_DATE($id,$_GET['date']);
}
?>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="user_stats.php">Users List</a>
            </li>
        </ul>
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12">
    <div class="form-group">
        <label class="control-label" for="selectError">Search By:-</label>
        <div class="controls">
            <select id="selectError" class="form-control" data-rel="chosen" style="width: 25%;" onchange="document.location.href = 'details.php?id=<?php echo $_GET['id'] ?>&search_by='+ this.value" >
                <option value="all">--Select--</option>
                <option value="week" <?php if($_GET['search_by']=='week'){ ?>selected<?php } ?> >Past Week</option>
                <option value="month" <?php if($_GET['search_by']=='month'){ ?>selected<?php } ?> >Past Month</option>
                <option value="date" <?php if($_GET['search_by']=='date'){ ?>selected<?php } ?> >Specific Date</option>
            </select>
        </div>
    </div>
        </div>
    </div>
    <?php if($_GET['search_by']=='date' && empty($_GET['date'])){ ?>
    <fieldset class="col-sm-12">
        <div class="form-group">
            <label class="control-label" for="date01">Date</label>
            <div class="controls row">
                <div class="input-group date col-sm-4">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control date-picker" id="date01" data-date-format="dd-mm-yyyy" style="width: 72%;" onchange="document.location.href = 'details.php?id=<?php echo $_GET['id'] ?>&search_by=date&date='+this.value" />
                </div>
            </div>
        </div>
    </fieldset>
    <?php } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Details <?php if($_GET['search_by']=='week'){ echo "- Last 7 days"; } elseif($_GET['search_by']=='month'){ echo "- Last Month"; } elseif(isset($_GET['date'])){ echo "- On ".$_GET['date']; } ?></h2>
                    <div class="box-icon">
                        <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th>Name of Ad</th>
                                <th>No. of Times</th>
                                <th>Money Earned</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            while($data=  mysql_fetch_array($detail))
                            {
                                $ad_list_by_id=GET_AD_VIEW($id,$data['ad_id']);
                                if($_GET['search_by']=='week'){
                                    $ad_list_by_id=GET_AD_VIEW_WEEK($id,$data['ad_id']);
                                }
                                if($_GET['search_by']=='month'){
                                    $ad_list_by_id=  GET_AD_VIEW_MONTH($id,$data['ad_id']);
                                }
                                if(isset($_GET['date']))
                                {
                                    $ad_list_by_id= GET_AD_VIEW_DATE($id,$data['ad_id'],$_GET['date']);
                                }
                                $ad_detail=  GET_AD_DATA($data['ad_id']);
                                $detail_ad=  mysql_fetch_array($ad_detail);
                                $client_data=GET_CLIENT_BY_ID($detail_ad['c_id']);
                                $c_data=  mysql_fetch_array($client_data);
                            ?>
                            <tr>
                                <td><?php echo $c_data['name']; ?></td>
                                <td><?php echo mysql_num_rows($ad_list_by_id); ?></td>
                                <?php 
                                $money=0;
                                while($fetch=  mysql_fetch_array($ad_list_by_id))
                                {
                                    $money=$money+$fetch['money_earned'];
                                }
                                ?>
                                <td><?php echo $money; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php') ?>
