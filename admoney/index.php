<?php
session_start();
include("config.php");
if(isset($_SESSION['user'])!='')
{
    header('Location: dashboard.php');
}
if(isset($_GET['action'])=='logout')
{
    $_SESSION['user']="";
    session_destroy();
    $msg='You are successfully logout';
}
if(isset($_POST['login']))
{
    $password=  md5($_POST['password']);
    $sql="Select * from admin_table where emailid='".$_POST['username']."' And password='".$password."'";
    $run=  mysql_query($sql);
    if(mysql_num_rows($run)==0)
    {
        $msg="Invalid User";
    }
    else{
        $result= mysql_fetch_array($run);
        $_SESSION['user']=$result['emailid'];
        header('Location:dashboard.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Ad Money</title>
	<meta name="description" content="">
	<meta name="author" content="Łukasz Holeczek">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="assets/css/bootstrap.min.css" rel="stylesheet">
	<!--link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet"-->
	<link id="base-style" href="assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="assets/css/style-responsive.css" rel="stylesheet">
	
	<!--[if lt IE 7 ]>
	<link id="ie-style" href="css/style-ie.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 8 ]>
	<link id="ie-style" href="css/style-ie.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 9 ]>
	<![endif]-->
	
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- start: Favicon -->
	
	<!-- end: Favicon -->
	<style type="text/css">
	body { background: url(assets/img/bg-login.jpg) !important; }
   </style>
	<script type="text/javascript">
  function validate_user()
	{
		
    var email_id=document.getElementById('email');
		var pass=document.getElementById('password').value;
    var flag=true;
		
		if(validateEmail(email_id)==false)
		{
     flag=false;
		 error="Please enter valid Email-Id";
		 document.getElementById('email').focus();
		}
		else if(pass=='')
		{
			flag=false;
			error="please enter password";
			document.getElementById('password').focus();
		}
		
		if(flag==false)
		{
			
			document.getElementById('error_msg1').style.display="block";
			document.getElementById('error_msg1').innerHTML=error;
			return false;
		}
		else
		{
			return true;
		}
					
	}
	
	function validateEmail(emailField)
	{
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
            
            return false;
        }

        return true;

  }
 
  </script>
		
		
</head>

<body>
		<div class="container-fluid">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<!--<a href="index.html"><i class="icon-home"></i></a>
						<a href="#"><i class="icon-cog"></i></a>-->
					</div>
					<h2>Login to your account</h2>
          <?php
					if(isset($msg))
					{ 
					?>
           <div id="error_msg" class="alert alert-error" style="width:305px;margin-left:14px;">
           <?php echo $msg;?>
           </div>
           <?php
					}
					 ?>
           <div id="error_msg1"  class="alert alert-error" style="width:305px;margin-left:14px;display:none;"></div>
					<form class="form-horizontal" action="" method="post" name="login_form" id="login_form" >
						<fieldset>
							
							<div class="input-prepend" title="Email-Id">
								<span class="add-on"><i class="icon-user"></i></span>
								<input class="input-large span10" name="username" id="email" type="text" placeholder="Email-Id" value=""/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="icon-lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="Password" value=""/>
							</div>
                                                        
							<div class="clearfix"></div>
							
							<!--<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>-->
                         
							<div class="button-login">
               				 <input type="submit" class="btn btn-primary" name="login" id="login" onClick="return validate_user();" value="Login"><i class="icon-off icon-white"></i> 
							</div>
							
							<div class="clearfix"></div>
					</form>
					<hr>
                                        <h3>Forgot Password?</h3>
				<p>
					No problem, <a href="forgot.php">click here</a> to get a new password.
				</p>
					
				</div><!--/span-->
			</div><!--/row-->
			
				</div><!--/fluid-row-->
				
	</div><!--/.fluid-container-->

	<!-- start: JavaScript-->

		<script src="assets/js/jquery-1.9.1.min.js"></script>
                <script src="assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="assets/js/bootstrap.min.js"></script>
	
		<script src="assets/js/jquery.cookie.js"></script>
	
		<script src='assets/js/fullcalendar.min.js'></script>
	
		<script src='assets/js/jquery.dataTables.min.js'></script>

		<script src="assets/js/excanvas.js"></script>
                <script src="assets/js/jquery.flot.min.js"></script>
                <script src="assets/js/jquery.flot.pie.min.js"></script>
                <script src="assets/js/jquery.flot.stack.js"></script>
                <script src="assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="assets/js/jquery.chosen.min.js"></script>
	
		<script src="assets/js/jquery.uniform.min.js"></script>
		
		<script src="assets/js/jquery.cleditor.min.js"></script>
	
		<script src="assets/js/jquery.noty.js"></script>
	
		<script src="assets/js/jquery.elfinder.min.js"></script>
	
		<script src="assets/js/jquery.raty.min.js"></script>
	
		<script src="assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="assets/js/jquery.gritter.min.js"></script>
	
		<script src="assets/js/jquery.imagesloaded.js"></script>
	
		<script src="assets/js/jquery.masonry.min.js"></script>
	
		<script src="assets/js/jquery.knob.js"></script>
	
		<script src="assets/js/jquery.sparkline.min.js"></script>

		<script src="assets/js/custom.js"></script>
		<!-- end: JavaScript-->
	
</body>
</html>
