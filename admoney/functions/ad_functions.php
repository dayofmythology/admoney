<?php
include ('../config.php');

function INSERT_AD($data,$file)
{
//    Array ( [selectclient] => 2 [start_date] => 17/11/2014 [expiry_date] => 27/11/2014 [max_view] => 12 [max_view_per_day] => 12 [money_per_view] => 12 [submit] => Submit )
//    Array ( [name] => dumbanddumberto.jpg [type] => image/jpeg [tmp_name] => /tmp/phpEUcCvx [error] => 0 [size] => 29568 )
    $start_date=  date('Y-m-d',strtotime($data['start_date']));
    $expiry_date=  date('Y-m-d',strtotime($data['expiry_date']));
    
    
    $namefile=time().'f'.$file['name'];
    $destination_path = BASE_DIR."functions/ad_files/" .basename($namefile);
    $source_path = $file['tmp_name'];
    move_uploaded_file($source_path, $destination_path);
        
    $filename = $file['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if($ext=='jpg' || $ext=='jpeg' || $ext=='png')
    {
        $filetype=0;
    }
    if($ext=='html' || $ext=='htm')
    {
        $filetype=2;
    }
    if($ext=='mp4' || $ext=='mpg' || $ext=='mpeg' || $ext=='3gp' || $ext=='m4v')
    {
        $filetype=1;
    }
    $sql="Insert into advertiesement(c_id, attached_file_path, file_type, start_date, expiry_date, max_view_for_ad, max_view_per_day, status, money_per_view) values('".$data['selectclient']."','".$namefile."','".$filetype."','".$start_date."','".$expiry_date."','".$data['max_view']."','".$data['max_view_per_day']."','1','".$data['money_per_view']."')";
    $result=  mysql_query($sql);
    return $result;
}


function GET_AD_LIST()
{
    $sql="Select * from advertiesement";
    $result=  mysql_query($sql);
    return $result;
}
function GET_CLIENT_BY_ID($id)
{
    $sql="Select name from client_info where c_id=".$id;
    $result=  mysql_query($sql);
    $info=  mysql_fetch_array($result);
    $name=$info['name'];
    return $name;
}
function UPDATE_STATUS_BYID($id,$status)
{
    $sql="UPDATE advertiesement SET status='".$status."' where ad_id='".$id."'";
    $result=mysql_query($sql);
    return $result;
}
function DELETE_AD_BY_ID($id)
{
    $chk_ads=  mysql_query("select * from ad_view_flag where ad_id='$id'");
    $no_of_usr=  mysql_num_rows($chk_ads);
    if($no_of_usr > 0){
        return FALSE;
    }else{
        $sql="Delete from advertiesement where ad_id=".$id;
        $result=  mysql_query($sql);
        return $result;
    }
    
}
function GET_CLIENT_LIST()
{
    $sql="Select * from client_info order by c_id";
    $result=  mysql_query($sql);
    return $result;
}
function GET_AD_BY_ID($id)
{
    $sql="Select * from advertiesement where ad_id=".$id;
    $result=  mysql_query($sql);
    return $result;
}
function UPDATE_AD($data,$file)
{
    $start_date=  date('Y-m-d',strtotime($data['start_date']));
    $expiry_date=  date('Y-m-d',strtotime($data['expiry_date']));
    
    if($file['name']=='')
    {
        $sql="Update advertiesement set c_id='".$data['selectclient']."', start_date='".$start_date."', expiry_date='".$expiry_date."', max_view_for_ad='".$data['max_view']."', max_view_per_day='".$data['max_view_per_day']."', money_per_view='".$data['money_per_view']."' where ad_id=".$data['id'];
    }else{
    $namefile=time().'f'.$file['name'];
    $destination_path = BASE_DIR."functions/ad_files/" .basename($namefile);
    $source_path = $file['tmp_name'];
    move_uploaded_file($source_path, $destination_path);
        
    $filename = $file['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if($ext=='jpg' || $ext=='jpeg' || $ext=='png')
    {
        $filetype=0;
    }
    if($ext=='html' || $ext=='htm')
    {
        $filetype=2;
    }
    if($ext=='mp4' || $ext=='mpg' || $ext=='mpeg' || $ext=='3gp' || $ext=='m4v' || $ext=='flv')
    {
        $filetype=1;
    }
    $sql="Update advertiesement set c_id='".$data['selectclient']."', attached_file_path='".$namefile."', file_type='".$filetype."', start_date='".$start_date."', expiry_date='".$expiry_date."', max_view_for_ad='".$data['max_view']."', max_view_per_day='".$data['max_view_per_day']."', money_per_view='".$data['money_per_view']."' where ad_id=".$data['id'];
    }
    $result=  mysql_query($sql);
    return $result;
}
function GET_MAXCOUNT_LIST($id)
{
    $count="Select count(*) as no_of_views from ad_view_flag where ad_id=".$id;
    $result=  mysql_query($count);
    $value=  mysql_fetch_array($result);
    return $value;
}
function GET_MAXCOUNT_PER_DAY($id)
{
    $now=  date('Y-m-d');
    $count="Select count(*) as no_of_views_a_day from ad_view_flag where ad_id='".$id."' and date_created='".$now."'";
    $result=  mysql_query($count);
    $value=  mysql_fetch_array($result);
    return $value;
}
function GET_VIEW_BY_ID($ad_id){
    $sql="Select * from ad_view_flag where ad_id='$ad_id'";
    $query=  mysql_query($sql);
    return $query;
}
function USERS_NUMBER($ad_id){
    $sql="Select DISTINCT uid from ad_view_flag where ad_id='$ad_id'";
    $query=  mysql_query($sql);
    return $query;
}
function GET_VIEW_OF_WEEK($ad_id){
    $lst_week=date('Y-m-d', strtotime('-7 days'));
    $sql="Select * from ad_view_flag where ad_id='$ad_id' and date_created > '$lst_week'";
    $query=  mysql_query($sql);
    return $query;
}
function GET_VIEW_OF_MONTH($ad_id){
    $lst_month=date('Y-m-d', strtotime('-30 days'));
    $sql="Select * from ad_view_flag where ad_id='$ad_id' and date_created > '$lst_month'";
    $query=  mysql_query($sql);
    return $query;
}
function GET_VIEW_OF_DATE($ad_id,$date){
    $date_formate=date('Y-m-d',  strtotime($date));
    $sql="Select * from ad_view_flag where ad_id='$ad_id' and date_created = '$date_formate'";
    $query=  mysql_query($sql);
    return $query;
}
?>
