<?php include ('header.php');
include_once ('config.php');
include ('functions/client_functions.php');
$client_list=  GET_CLIENT_LIST();
?>
<script>
    $(document).ready(function (){
       $('#example').dataTable({ "bLengthChange": true,
"aoColumnDefs": [{ "bSearchable": false, "aTargets": [6]}] // this line does your work!
});
    });
</script>
<div id="content" class="col-sm-10">
    <div>
        <hr>
        <ul class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="clients.php">Client List</a>
            </li>
        </ul>
        <hr>
    </div>
    <div style="color: red;text-align: center"><?php if(isset($_SESSION['msg'])) { echo $_SESSION['msg']; unset($_SESSION['msg']); } ?></div>
    <a title="Add New Client" class="btn btn-primary" href="add_client.php"><span class="hidden-sm">Add Client</span></a>
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header" data-original-title>
                    <h2><i class="fa fa-user"></i><span class="break"></span>Clients List</h2>
                    <div class="box-icon">
                        <a href="clients.php#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                    <div class="box-content">
                        <table  id="example" class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Contact Number</th>
                                    <th>Email</th>
                                    <th>Website Url</th>
                                    <th>Registration Date</th>
                                    <th>Status</th>
                                    <th style="width: 11%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($get_list=  mysql_fetch_array($client_list)){ ?>
                                <tr>
                                    <td><?php echo $get_list['name']; ?></td>
                                    <td><?php echo $get_list['address']; ?></td>
                                    <td><?php echo $get_list['contact_no']; ?></td>
                                    <td><?php echo $get_list['email']; ?></td>
                                    <td><?php echo $get_list['weburl']; ?></td>
                                    <td><?php 
                                    $date=explode(" ",$get_list['date_created']);
                                    echo date('d-m-Y',strtotime($date[0]));
                                    ?></td>
                                    <td id="column3_search">
                                        <?php
                                        if($get_list['status']==1){ ?>
                                        <a title="Change Status" href="clients.php?action=status&c_id=<?=$get_list['c_id']?>&status=0"><span class="label label-success">Active</span></a>
                                       <?php } else { ?>
                                        <a title="Change Status" href="clients.php?action=status&c_id=<?=$get_list['c_id']?>&status=1"><span class="label label-default">Inactive</span></a>
                                       <?php } ?>
                                        
                                    </td>
                                    <td>
                                        <a title="Edit" class="btn btn-info" href="add_client.php?c_id=<?=$get_list['c_id']?>">
                                            <i class="fa fa-edit "></i>
                                        </a>
                                        <a title="Delete" class="btn btn-danger" onClick="if(!confirm('Are you sure, You want delete this record?')){return false;}" href="clients.php?action=delete&c_id=<?=$get_list['c_id']?>">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<?php
if($_GET['action']=='status')
{
	$result=UPDATE_STATUS_BYID($_GET['c_id'],$_GET['status']);
	if($result > 0)
	{
		$_SESSION['msg']="Status update successfully !";
                $url=$DIR_PATH."clients.php";
                echo "<script>window.top.location='".$url."'</script>";
	} 
}
if($_GET['action']=='delete')
{
$del=DELETE_CLIENT_BY_ID($_GET['c_id']);
if($del > 0)
{
    $_SESSION['msg']="Record deleted successfully !";
    $url=$DIR_PATH."clients.php";
    echo "<script>window.top.location='".$url."'</script>";
} elseif($del == FALSE) {
    $_SESSION['msg']="Could not delete client !!<br> Client has Advertisements in Adverts Section !!";
    $url=$DIR_PATH."clients.php";
    echo "<script>window.top.location='".$url."'</script>";
}
}
?>
<?php include ('footer.php'); ?>

